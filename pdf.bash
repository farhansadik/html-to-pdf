#!/bin/bash 

function loop() {

	# for many copies 

	for i in {1..10}; do {
		declare url=https://github.com/htr-tech/bash2mp4
		wkhtmltopdf -s A4 --disable-smart-shrinking --zoom 1.0 $url output_file$i.pdf
	} 
	done;
}

main_page=https://wkhtmltopdf.org/
source_code=https://github.com/wkhtmltopdf/wkhtmltopdf


wkhtmltopdf -s A4 --background --no-images $main_page main_page.pdf 
wkhtmltopdf --page-size A4 --no-background --page-offset 0 $source_code github.pdf 
